import {Config} from 'tailwindcss'

const config = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors:{
        primaryBackground:'var(--primary-background)',
        primaryForeground:'var(--primary-foreground)',
        primary:'var(--primary)',
        moonBorder:'var(--moon-border)',
        inputColor:'var(--input-color)',
        hrColor:'var(--hr)',
      },
      fontFamily:{
        inter:'Inter',
        lora:'Lora',
        inconsolata:'Inconsolata',
      }
    },
  },
  darkMode:['class'],
  plugins: [],
} satisfies Config

export default config

