import { Routes } from '@angular/router';
import { WordMeaningComponent } from './word-meaning/word-meaning.component';
import { ErrorComponent } from './error/error.component';

export const routes: Routes = [
  {path:'', redirectTo:'/search?query=keyword', pathMatch:'full', },
  {path:'search', component: WordMeaningComponent, data:{animation:'querying'}},
  {path:'**', component: ErrorComponent},      
];


