import { Component, EventEmitter, OnInit, Output,  } from "@angular/core";

import { NgClass, NgOptimizedImage } from '@angular/common'
import { ToggleComponent } from "../toggle/toggle.component";
import {MatIconModule} from '@angular/material/icon'
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { ClickOutsideDirective } from "../../services/clickOutside.directive";


enum SelectedFont {
  serif = 'serif',
  sansSerif = 'sans serif',
  mono = 'mono'
}


@Component({
  selector:'navbar',
  standalone:true,
  templateUrl:'./navbar.component.html',
  imports:[NgOptimizedImage, ToggleComponent,MatIconModule, NgClass, ClickOutsideDirective],
  animations:[
    trigger('openClose', [
      state('open', style({
        opacity:1,
        // visi
        display:'flex'
      })),

      state(
        'closed',
        style({
          // height: '0px',
          opacity: 0,
          display:'none'
        }),
      ),
      transition('open => closed', [animate('0.1s')]),
      transition('closed => open', [animate('0.1s')]),
    ])
  ]
})

export class NavbarComponent  {
  selectedFont:SelectedFont | 'sans serif' | 'serif' | 'mono' = SelectedFont.sansSerif
  isFontSelectionOpen = false
  @Output() selectFontEvent = new EventEmitter<string>();

  
  toggleFontSelection(){
    this.isFontSelectionOpen = !this.isFontSelectionOpen
  }

 FontSelect(font:'sans serif' | 'serif' | 'mono') {
  this.selectedFont = font;
  console.log('close to emmit ', this.selectedFont);
  this.isFontSelectionOpen = false;
  this.selectFontEvent.emit(font);
 }
 closeFontSelection() {
  this.isFontSelectionOpen = false;
}
openFontSelection () {
  this.isFontSelectionOpen
}
}