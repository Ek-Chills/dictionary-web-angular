import { Component } from '@angular/core';
import { ChildrenOutletContexts, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import {MatIconModule} from '@angular/material/icon'
import { SearchComponent } from './search/search.component';
import { WordMeaningComponent } from './word-meaning/word-meaning.component';
import { NgClass } from '@angular/common';
import { fadeInAnimation } from './animations';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavbarComponent,MatIconModule,WordMeaningComponent, SearchComponent, NgClass],
  templateUrl: './app.component.html',
  animations:[fadeInAnimation],
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'dictionary-web-app';
  selectedFont:string = 'san serif'

  constructor (private contexts:ChildrenOutletContexts) {}

  getRouteAnimationData() {
    return this.contexts.getContext('primary')?.route?.snapshot?.data?.['animation'];
  }

  changeFont(selectedFont:string) {
    console.log('from parent', selectedFont);
    this.selectedFont = selectedFont;    
  }
}
