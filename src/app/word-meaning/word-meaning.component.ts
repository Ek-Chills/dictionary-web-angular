import { Component, ElementRef, OnInit, ViewChild, viewChild } from '@angular/core';
import { UseFetchService } from '../../services/usefetch.service';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { LucideAngularModule, File, Home, Menu, UserCheck } from 'lucide-angular';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { AnimationOptions, LottieComponent } from 'ngx-lottie';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Observable, Subject } from 'rxjs';


interface Error {
  message: string;
  title: string;
  resolution:string;
  isError:boolean;
}

@Component({
  selector: 'app-word-meaning',
  standalone: true,
  imports: [NgOptimizedImage, CommonModule, LottieComponent, RouterModule],
  templateUrl: './word-meaning.component.html',
  styleUrls: ['./word-meaning.component.css'],
  animations:[
    trigger('mountAnimation', [
      state('open', style({
        opacity:1,
        width:'100%'
        // visi
      })),

      state(
        'closed',
        style({
          // height: '0px',
          opacity: 0,
          width:'0'
        }),
      ),
      transition('open => closed', [animate('0.5s')]),
      transition('closed => open', [animate('0.5s')]),
    ])
  ]
})
export class WordMeaningComponent implements OnInit {
  result: Meaning = [];
  pathObservableSubject = new Subject<string>()
  pathObservable$:Observable<string> = this.pathObservableSubject.asObservable()
  @ViewChild('audioRef', {static:false}) audioRef?:ElementRef<HTMLAudioElement>
  animationName = ''
  lottieOptions:AnimationOptions = {
    path:'../../assets/sad-emoji.json'
  }
  query: string = ''
  isFetching:boolean = false
  error:Error = {
    isError:false,
    message: '',
    resolution:'',
    title: ''
  }

  constructor(private useFetchService: UseFetchService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.subscribeToQueryParams();
    this.fetchData();
    this.addAnimation()
    this.pathObservable$.subscribe({
      next:(res) => {
        console.log('observe', res);
        
        this.animationName = res
      }
    })    
  }

  
  private subscribeToQueryParams(): void {
    this.route.queryParams.subscribe((params) => {
      this.query = params['query'];
      this.fetchData();
    });
  }

  addAnimation() {
    this.pathObservable$.subscribe({
      next:(res) => {
        this.animationName = res
      }
    })
  }

  playAudio() {
   this.audioRef?.nativeElement.play()
  }

  private fetchData(): void {
    this.pathObservableSubject.next('closed')
    this.isFetching = true;
    this.error = {
      ...this.error,
      isError:false
    }
    this.useFetchService.getWord(this.query).subscribe({
      next: (res) => {
        console.log(res);
        
        this.pathObservableSubject.next('open')
        this.result = res;
        console.log(this.result);
        this.isFetching = false
      },
      error:(err) => {
        this.isFetching = false
  
        if(err instanceof HttpErrorResponse) {
          if(err.status === 404) {
            this.error = {
              isError: true,
              message: err.error.message,
              resolution: err.error.resolution,
              title: err.error.title
            }
          }
        }
        console.log(err);
        
        console.log('an error occured');
        
      },
      complete: () => {
        this.isFetching = false
      }
    });
  }
}