import { Component, ElementRef, Inject, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {MatIconModule} from '@angular/material/icon'
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-search',
  standalone: true,
  imports: [ReactiveFormsModule, MatIconModule, CommonModule],
  templateUrl: './search.component.html',
  styleUrl: './search.component.css'
})
export class SearchComponent implements OnInit, AfterViewInit {

  @ViewChild('inputRef', {static:false})inputElement?:ElementRef<HTMLInputElement>;

  isRouting = true

  searchForm = new FormGroup({
    query: new FormControl('', Validators.required)
  })

  constructor(private route:ActivatedRoute, private router:Router) {}

  ngOnInit(): void {
    // if(this.inputElement) {
    //   console.log(this.inputElement);
    //   this.inputElement.nativeElement.focus()
    // }
    this.inputElement?.nativeElement.focus()

    this.route.queryParams.subscribe(params => {
      console.log(params['query']);
      
    this.searchForm.setValue({query:params['query']})     
    })
  }

  ngAfterViewInit(): void {
    this.inputElement?.nativeElement.focus()
  }


  

  onSubmit() {
    this.isRouting = true
    this.router.navigateByUrl(`/search?query=${this.searchForm.value.query}`, {replaceUrl:true}).then(() => {
      this.isRouting = true
    }).finally(() => {
      this.isRouting = false
    })
  }

  onKeyDown(e:KeyboardEvent) {
    if(e.key === 'Enter') {
      this.onSubmit()
    }
  }


}
