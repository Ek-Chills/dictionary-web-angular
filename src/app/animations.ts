import { animate, animateChild, group, query, style, transition, trigger } from "@angular/animations";

export const fadeInAnimation = (
    trigger('querying', [
      transition('querying => *', [
        style({ opacity: 0 }),
        animate('5s ease-in', style({ opacity: 1 })),
        query(':enter', [
          style({ opacity: 0 }),
          animate('5s ease-in', style({ opacity: 1 }))
        ], { optional: true })
      ]),
      transition('* => querying', [
        animate('5s ease-out', style({ opacity: 0 })),
        query(':leave', [
          animate('5s ease-out', style({ opacity: 0 }))
        ], { optional: true })
      ])
    ])  
)