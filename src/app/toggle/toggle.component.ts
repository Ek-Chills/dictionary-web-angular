import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { NgClass } from "@angular/common";
import { Subject } from "rxjs";

@Component({
  selector: 'toggle',
  imports:[NgClass],
  template: `
    <div
      (click)="toggleDarkMode()"
      class="w-[40px] h-[20px] rounded-r-full bg-primary p-[3px] rounded-l-full flex items-center justify-start cursor-pointer transition-all duration-300"
      [ngClass]="{ 'justify-end': isDarkMode }"
    >
      <span class="w-3.5 h-3.5 bg-white rounded-full"></span>
    </div>
  `,
  standalone: true,
})
export class ToggleComponent implements OnInit {
  @Input() isDarkMode: boolean = false;
  private darkModeSubject = new Subject<boolean>();
  darkMode$ = this.darkModeSubject.asObservable();

  constructor() {
    console.log('constructor called');  
  }
  
  ngOnInit(): void {
    const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
    this.isDarkMode = mediaQuery.matches;
    this.updateDarkMode();
    console.log('dark', this.isDarkMode);
    this.darkMode$.subscribe(theme => {
      this.isDarkMode = theme;
      this.updateDarkMode();
    })
  }



  
  

  toggleDarkMode() {
    console.log('toggled');
    this.isDarkMode = !this.isDarkMode;
    this.darkModeSubject.next(this.isDarkMode)
    console.log(this.isDarkMode);
  }

  updateDarkMode() {
    document.documentElement.classList.toggle('dark', this.isDarkMode);
  }
}