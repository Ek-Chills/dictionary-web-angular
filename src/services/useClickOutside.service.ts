import { ElementRef, Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})

export class ClickOutsideService {

  handleClickOutside(el:ElementRef<HTMLDivElement>, handler:(event:MouseEvent | TouchEvent) => void) {
    document.addEventListener('mousedown', (e) => {
      if(el.nativeElement.contains(e.target as Node) || !el) {
        return
      }
      handler(e)
    })

    document.addEventListener('touchstart', (e) => {
      if(el.nativeElement.contains(e.target as Node) || !el) {
        return
      }
      handler(e)
    })
  }
}