import { Directive, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
const openingElement = document.getElementById('opener')
@Directive({
  selector: '[clickOutside]',
  standalone:true
})
export class ClickOutsideDirective {
  @Output() clickOutside = new EventEmitter<void>();

  constructor(private elementRef: ElementRef) {}

  @HostListener('document:mousedown', ['$event.target'])
  onClick(target: any) {
    const clickedInside = this.elementRef.nativeElement.contains(target) || openingElement;
    if (!clickedInside) {
      this.clickOutside.emit();
    }
  }
}
