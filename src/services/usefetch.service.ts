import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UseFetchService {

constructor(private http:HttpClient) {}
resultsData = [] as any
  getWord(query:string):Observable<Meaning> {
    return this.http.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${query}`) as Observable<Meaning>;
  }
  
  
}
  // methods to retrieve and return data
